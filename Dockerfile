FROM openjdk:18

ARG JAR_FILE=build/libs/test-0.0.1-SNAPSHOT.jar

ADD ${JAR_FILE} test.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "test.jar"]
