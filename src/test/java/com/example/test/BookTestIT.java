package com.example.test;

import com.graphql.spring.boot.test.GraphQLResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
public class BookTestIT extends TestApplicationTests {

    @BeforeEach
    void init() throws IOException {
        graphQLTestTemplate.postForResource("graphql/author/saveAuthor.graphqls");
    }

    @Test
    void saveBook() throws IOException {
        graphQLTestTemplate.postForResource("graphql/book/saveBook.graphqls")
                .isOk();
    }

    @Test
    void getAllBooks() throws IOException {

        GraphQLResponse graphQLResponse =
                graphQLTestTemplate.postForResource("graphql/book/getAllBooks.graphqls");

        graphQLResponse.isOk();
        assertEquals("Live", graphQLResponse.get("$.data.getAllBooks.[0].title"));
        assertEquals("Home", graphQLResponse.get("$.data.getAllBooks.[1].title"));
        assertEquals("City", graphQLResponse.get("$.data.getAllBooks.[2].title"));
    }

    @Test
    void getAuthor() throws IOException {
        GraphQLResponse graphQLResponse =
                graphQLTestTemplate.postForResource("graphql/book/getBooksByAuthor.graphqls");

        graphQLResponse.isOk();
        assertEquals("Sem", graphQLResponse.get("$.data.getAuthor.name"));
        assertEquals("City", graphQLResponse.get("$.data.getAuthor.books[0].title"));

    }
}
