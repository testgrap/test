package com.example.test;

import com.graphql.spring.boot.test.GraphQLResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
public class AuthorTestIT extends TestApplicationTests {

    @BeforeEach
    void init() throws IOException {
        graphQLTestTemplate.postForResource("graphql/author/beforeEach/beforeSaveAuthor.graphqls");
    }

    @Test
    void saveAuthor() throws IOException {
        graphQLTestTemplate.postForResource("graphql/author/saveAuthor.graphqls")
                .isOk();
    }

    @Test
    void getAuthor() throws IOException {
        GraphQLResponse graphQLResponse =
                graphQLTestTemplate.postForResource("graphql/author/getAuthor.graphqls");

        graphQLResponse.isOk();
        assertEquals("Alex", graphQLResponse.get("$.data.getAuthor.name"));
        assertEquals("Cat", graphQLResponse.get("$.data.getAuthor.books.[0].title"));
        assertEquals("Dog", graphQLResponse.get("$.data.getAuthor.books.[1].title"));


    }
}
