package com.example.test.repository;

import com.example.test.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long> {

    Optional<Book> findBookByTitle(String title);

    @Query("select b from Book b join fetch b.authors a where a.name = :name")
    List<Book> findBooksByAuthor(@Param(value = "name") String name);
}
