package com.example.test.repository;

import com.example.test.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AuthorRepository extends JpaRepository<Author, Long> {

    Optional<Author> findByName(String name);

    @Query("select a from Author a join fetch a.books b where b.title = :title")
    List<Author> findAuthorByNameBook(@Param(value = "title") String title);
}
