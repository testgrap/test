package com.example.test.graphQLService.query.author;

import com.example.test.entity.Author;
import com.example.test.repository.AuthorRepository;
import graphql.GraphQLException;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class AuthorQuery implements GraphQLQueryResolver {

    private final AuthorRepository authorRepository;

    @Transactional
    public Author getAuthor(String name) {
        return authorRepository.findByName(name)
                .orElseThrow(() -> new GraphQLException("Нет автора с таким именем "));
    }
}
