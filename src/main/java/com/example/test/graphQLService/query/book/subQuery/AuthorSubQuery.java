package com.example.test.graphQLService.query.book.subQuery;

import com.example.test.entity.Author;
import com.example.test.entity.Book;
import com.example.test.repository.AuthorRepository;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class AuthorSubQuery implements GraphQLResolver<Book> {

    private final AuthorRepository authorRepository;

    public List<Author> authors(Book book) {
        return authorRepository.findAuthorByNameBook(book.getTitle());
    }
}
