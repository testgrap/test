package com.example.test.graphQLService.query.author.subQuery;

import com.example.test.entity.Author;
import com.example.test.entity.Book;
import com.example.test.repository.BookRepository;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class BookSubQuery implements GraphQLResolver<Author> {

    private final BookRepository bookRepository;

    public List<Book> books(Author author) {
        return bookRepository.findBooksByAuthor(author.getName());
    }
}
