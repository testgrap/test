package com.example.test.graphQLService.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
public class InputAuthor {

    private String name;

    @Builder.Default
    private List<InputBook> books= new ArrayList<>();
}
