package com.example.test.graphQLService.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
public class InputBook {

    private String title;

    @Builder.Default
    private List<InputAuthor> authors = new ArrayList<>();
}
