package com.example.test.graphQLService.mutation.author;

import com.example.test.entity.Author;
import com.example.test.entity.Book;
import com.example.test.graphQLService.model.InputBook;
import com.example.test.repository.AuthorRepository;
import com.example.test.repository.BookRepository;
import graphql.GraphQLException;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class AuthorMutation implements GraphQLMutationResolver {

    private final AuthorRepository authorRepository;

    private final BookRepository bookRepository;

    @Transactional
    public Boolean saveAuthor(String name, List<InputBook> inputBooks) {
        boolean isExistAuthorInDataBase = authorRepository.findByName(name).isPresent();
        if (isExistAuthorInDataBase) {
            throw new GraphQLException("Автор с таким именем уже существует");
        }

        Author authorBuild = Author.builder().name(name).build();

        List<Book> booksSavedInDataBase = getBooksSavedInDataBase(inputBooks);
        List<Book> uniqueBooks = getDistinctBooks(booksSavedInDataBase, mappingToBook(inputBooks));
        List<Book> authorWithBook = getAuthorWithBook(uniqueBooks, authorBuild);

        authorBuild.setBooks(authorWithBook);
        authorRepository.save(authorBuild);
        return true;
    }

    private List<Book> getBooksSavedInDataBase(List<InputBook> inputBooks) {
        return inputBooks.stream()
                .map(inputBook -> bookRepository.findBookByTitle(inputBook.getTitle()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private List<Book> getDistinctBooks(List<Book> booksSavedInDataBase, List<Book> inputBooks) {
        List<Book> localInputBooks = new ArrayList<>(inputBooks);

        for (Book saveBook : booksSavedInDataBase) {
            for (Book inputBook : inputBooks) {
                if (saveBook.getTitle().equals(inputBook.getTitle())) {
                    localInputBooks.remove(inputBook);
                }
            }
        }

        return Stream.of(booksSavedInDataBase, localInputBooks)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    List<Book> mappingToBook(List<InputBook> inputBooks) {
        return inputBooks.stream()
                .map(inputBook -> Book.builder().title(inputBook.getTitle()).build())
                .collect(Collectors.toList());
    }

    List<Book> getAuthorWithBook(List<Book> books, Author author) {
        return books.stream()
                .peek(book -> book.setAuthors(List.of(author)))
                .collect(Collectors.toList());
    }

}
