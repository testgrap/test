package com.example.test.graphQLService.mutation.book;

import com.example.test.entity.Author;
import com.example.test.entity.Book;
import com.example.test.graphQLService.model.InputAuthor;
import com.example.test.repository.AuthorRepository;
import com.example.test.repository.BookRepository;
import graphql.GraphQLException;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class BookMutation implements GraphQLMutationResolver {

    private final BookRepository bookRepository;

    private final AuthorRepository authorRepository;

    @Transactional
    public Boolean saveBook(String title, List<InputAuthor> inputAuthors) {
        boolean isExistBookInDataBase = bookRepository.findBookByTitle(title).isPresent();
        if (isExistBookInDataBase) {
            throw new GraphQLException("Книга с таким title уже существует");
        }
        if (inputAuthors.size() < 1) {
            throw new GraphQLException("У книги должен быть автор");
        }

        Book buildBook = Book.builder().title(title).build();
        List<Author> authorsSavedInDataBase = getAuthorsSavedInDataBase(inputAuthors);
        List<Author> uniqueAuthor = getDistinctAuthor(authorsSavedInDataBase, mappingToAuthor(inputAuthors));
        List<Author> authorWithBook = getAuthorWithBook(uniqueAuthor, buildBook);

        buildBook.setAuthors(authorWithBook);

        bookRepository.save(buildBook);
        return true;
    }


    private List<Author> mappingToAuthor(List<InputAuthor> inputAuthors) {
        return inputAuthors.stream()
                .map(inputAuthor -> Author.builder().name(inputAuthor.getName()).build())
                .collect(Collectors.toList());
    }

    private List<Author> getAuthorsSavedInDataBase(List<InputAuthor> inputAuthors) {
        return inputAuthors.stream()
                .map(inputAuthor -> authorRepository.findByName(inputAuthor.getName()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private List<Author> getDistinctAuthor(List<Author> listAuthorSavedInDataBase, List<Author> inputAuthors) {
        List<Author> localListAuthors = new ArrayList<>(inputAuthors);

        for (Author autSave : listAuthorSavedInDataBase) {
            for (Author autInput : inputAuthors) {
                if (autSave.getName().equals(autInput.getName())) {
                    localListAuthors.remove(autInput);
                }
            }
        }

        return Stream.of(listAuthorSavedInDataBase, localListAuthors)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<Author> getAuthorWithBook(List<Author> authorList, Book book) {
        return authorList.stream()
                .peek(author -> author.getBooks().add(book))
                .collect(Collectors.toList());
    }

}
